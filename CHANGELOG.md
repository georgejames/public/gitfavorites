# Change Log
All notable changes to "Git Favorites" will be documented in this file. We aim use the guidance from [Keep a Changelog](http://keepachangelog.com/) to structure this file.

## [0.1.1] - 2020-08-17
- Fix title of README.
- Fix LICENSE date.

## [0.1.0] - 2020-08-17
- Initial release.