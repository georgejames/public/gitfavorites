# Git Favorites README

This extension became obsolete when VS Code added its own Pull and Push menus, so it was unpublished from Marketplace on 08-Jun-2021.
---

**Git Favorites** is a simple extension that adds the Pull and Push options to the context menu of a git repository's entry on the SCM view.

## Features

When version 1.48 of VS Code [restructured the context menu](https://code.visualstudio.com/updates/v1_48#_git-overflow-menu-cleanup) to use submenus, some users found it inconvenient to have to navigate further to reach the 'Pull' and 'Push' options. This simple extension adds them to the top of the menu.

## Extension Settings

There are currently no configuration settings for **Git Favorites**.

## Known Issues

None at this time.
